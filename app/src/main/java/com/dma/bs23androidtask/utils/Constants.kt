package com.dma.bs23androidtask.utils

object Constants {
    const val BASE_URL = "https://api.github.com/search/"
    const val APP_ID = "com.dma.bs23androidtask"
    const val QUERY = "Android"
    const val SORT = "star"
    const val NETWORK_TIMEOUT = 60L
}