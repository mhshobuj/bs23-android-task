package com.dma.bs23androidtask.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dma.bs23androidtask.repository.MainRepository
import com.dma.bs23androidtask.response.GitRepoListResponse
import com.dma.bs23androidtask.utils.DataStatus
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: MainRepository) : ViewModel() {

    //viewModel for GitRepoList
    private val _gitRepoList = MutableLiveData<DataStatus<GitRepoListResponse>> ()
    val gitGitRepoList: LiveData<DataStatus<GitRepoListResponse>> get() = _gitRepoList
    fun getGitRepoList(app_id: String, query: String, sort: String) =viewModelScope.launch {
        repository.getGitRepoList(app_id, query, sort).collect{
            _gitRepoList.value = it
        }
    }
}