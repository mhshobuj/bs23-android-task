package com.dma.bs23androidtask.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.dma.bs23androidtask.adapter.GitRepoListAdapter
import com.dma.bs23androidtask.databinding.ActivityMainBinding
import com.dma.bs23androidtask.utils.Constants
import com.dma.bs23androidtask.utils.DataStatus
import com.dma.bs23androidtask.utils.isVisible
import com.dma.bs23androidtask.viewModel.MainViewModel
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()
    private lateinit var gitRepoListAdapter: GitRepoListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        gitRepoListAdapter = GitRepoListAdapter { clickedItem ->
            val intent = Intent(this, DetailsActivity::class.java)
            // Convert the clickedItem (assuming it's of type GitRepoListResponse) to a JSON string
            val json = Gson().toJson(clickedItem)
            // Add the JSON string to the Intent as an extra
            intent.putExtra("jsonData", json)
            // Start the DetailsActivity
            startActivity(intent)
        }

        getGitRepoList()

        binding.recyclerView.layoutManager = LinearLayoutManager(this).apply {
            orientation = LinearLayoutManager.VERTICAL
        }

        binding.recyclerView.adapter = gitRepoListAdapter
    }

    private fun getGitRepoList() {
        lifecycleScope.launch {
            binding.apply {
                viewModel.getGitRepoList(Constants.APP_ID, Constants.QUERY, Constants.SORT)
                viewModel.gitGitRepoList.observe(this@MainActivity){
                    when(it.status){
                        DataStatus.Status.LOADING ->{
                            pBarLoading.isVisible(true, recyclerView)
                        }

                        DataStatus.Status.SUCCESS ->{
                            // Pass retrieved data to the adapter
                            pBarLoading.isVisible(false, recyclerView)
                            gitRepoListAdapter.submitList(it.data!!.items)
                        }

                        DataStatus.Status.ERROR ->{
                            pBarLoading.isVisible(false, recyclerView)
                            Toast.makeText(this@MainActivity, "There is something wrong!", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }
    }
}