package com.dma.bs23androidtask.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import coil.load
import com.dma.bs23androidtask.R
import com.dma.bs23androidtask.databinding.ActivityDetailsBinding
import com.dma.bs23androidtask.response.GitRepoListResponse
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

@AndroidEntryPoint
class DetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Retrieve the JSON string from the Intent
        val json = intent.getStringExtra("jsonData")
        // Convert the JSON string back to GitRepoListResponse object using Gson
        val clickedItem = Gson().fromJson(json, GitRepoListResponse.Item::class.java)

        binding.apply {
            details = clickedItem
            imgAvaterLogo.load(clickedItem.owner?.avatarUrl){
                crossfade(true)
                crossfade(500)
                placeholder(R.drawable.ic_launcher_background)
                error(R.drawable.ic_launcher_background)
            }
            txtDateTitle.text = clickedItem!!.pushedAt?.let { convertUtcDateToCustomFormat(it) }
        }
    }

    private fun convertUtcDateToCustomFormat(utcDate: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
        val outputFormat = SimpleDateFormat("MMM dd, yyyy HH:mm", Locale.US)
        // Parse the UTC date string
        val date: Date = inputFormat.parse(utcDate) ?: return ""
        // Format the date to the desired custom format
        return outputFormat.format(date)
    }
}