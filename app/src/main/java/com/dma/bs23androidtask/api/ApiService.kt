package com.dma.bs23androidtask.api

import com.dma.bs23androidtask.response.GitRepoListResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiService {
    @GET("repositories")
    suspend fun getGitRepoList(
        @Header("app_package_id") appPackageId: String,
        @Query("q") q: String,
        @Query("sort") sort: String
    ): Response<GitRepoListResponse>
}