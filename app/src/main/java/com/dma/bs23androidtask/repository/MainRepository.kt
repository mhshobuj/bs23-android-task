package com.dma.bs23androidtask.repository

import com.dma.bs23androidtask.api.ApiService
import com.dma.bs23androidtask.utils.DataStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class MainRepository @Inject constructor(private val apiService: ApiService) {
    //get GitRepoList
    suspend fun getGitRepoList(app_id: String, query: String, sort: String) = flow {
        emit(DataStatus.loading())
        val result = apiService.getGitRepoList(app_id, query, sort)
        when (result.code()) {
            200 -> {
                emit(DataStatus.success(result.body()))
            }
            400 -> {
                emit(DataStatus.error(result.message()))
            }
            500 -> {
                emit(DataStatus.error(result.message()))
            }
        }
    }.catch {
        emit(DataStatus.error(it.message.toString()))
    }.flowOn(Dispatchers.IO)
}