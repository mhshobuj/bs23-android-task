package com.dma.bs23androidtask.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.dma.bs23androidtask.R
import com.dma.bs23androidtask.databinding.RecyclerItemBinding
import com.dma.bs23androidtask.response.GitRepoListResponse

class GitRepoListAdapter (private val onItemClick: (GitRepoListResponse.Item) -> Unit) : ListAdapter<GitRepoListResponse.Item, GitRepoListAdapter.MyViewHolder>(GitRepoListDiffUtil()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = RecyclerItemBinding.inflate(inflater, parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class MyViewHolder(private val binding: RecyclerItemBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    onItemClick.invoke(item) // Replace 'someString' with the string you want to pass
                }
            }
        }
        fun bind(item: GitRepoListResponse.Item) {
            binding.repoItem = item
            binding.imgUser.load(item.owner?.avatarUrl){
                crossfade(true)
                crossfade(500)
                placeholder(R.drawable.ic_launcher_background)
                error(R.drawable.ic_launcher_background)
            }
        }
    }
}

class GitRepoListDiffUtil : DiffUtil.ItemCallback<GitRepoListResponse.Item>(){
    override fun areItemsTheSame(
        oldItem: GitRepoListResponse.Item,
        newItem: GitRepoListResponse.Item
    ): Boolean {
        return  oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: GitRepoListResponse.Item,
        newItem: GitRepoListResponse.Item
    ): Boolean {
        return oldItem == newItem
    }
}
